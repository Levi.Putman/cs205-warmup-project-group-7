# CS205 Warmup Project Group 7
Authors: Eyu, Juli, Nikhil, Levi

## About
This program is a simple query interface that allows users to query information about CS courses and teachers at UVM.

## Getting started 
After cloning the repository to your local machine, you can run `python course_querier.py`

## IMPORTANT
This program uses the `match` statement in python which requires python version 3.10 or higher.

